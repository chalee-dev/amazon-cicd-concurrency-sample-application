#!/usr/bin/env sh
 
set -eu

############################################################
# Set log levels for different levels of verbosity
############################################################
ERROR=1
DEBUG=1
INFO=1
FAILURE_CODE=255

############################################################
# Helper functions for logging messages with different
# verbosity levels
# ARGUMENTS:
#    String(s) to dump as terminal output
# OUTPUTS:
#    Write string to stdout
# RETURNS:
#    Exit code of the bash builtin
############################################################

function __msg_error() {
    [[ "${ERROR}" == "1" ]] && echo -e "[ERROR]: $*"
}
 
function __msg_debug() {
    [[ "${DEBUG}" == "1" ]] && echo -e "[DEBUG]: $*"
}
 
function __msg_info() {
    [[ "${INFO}" == "1" ]] && echo -e "[INFO]: $*"
}

############################################################
# Validates the existence of all required vars
# ARGUMENTS:
#    None
# OUTPUTS:
#    Writes error string to stdout
# RETURNS:
#    0 if successful, 255 if error
############################################################
 
function check_mandatory_vars() {
    if [[ -z ${AWS_CODECOMMIT_REPOSITORY_URL} ]] ||  \
              [[ -z ${GIT_HTTPS_USERNAME_DEV} ]] ||  \
              [[ -z ${GIT_HTTPS_PASSWORD_DEV} ]] ||  \
              [[ -z ${GIT_HTTPS_USERNAME_MAIN} ]] || \
              [[ -z ${GIT_HTTPS_PASSWORD_MAIN} ]]; then
      _msg_error "Mandatory var(s) not present"
      exit ${FAILURE_CODE}
    else
     __msg_info "All Mandatory vars present"
    fi
}
 
############################################################
# Switch variable values based on git branch
# ARGUMENTS:
#    None
# OUTPUTS:
#    Writes a message to stdout in case of error
# RETURNS:
#    0 if successful, 255 if error 
############################################################
 
function check_branch() {
    if [[ "${CI_COMMIT_REF_NAME}" == "develop" ]]; then
      GIT_HTTPS_USERNAME="${GIT_HTTPS_USERNAME_DEV}"
      GIT_HTTPS_PASSWORD="${GIT_HTTPS_PASSWORD_DEV}"
    elif [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then
      GIT_HTTPS_USERNAME="${GIT_HTTPS_USERNAME_MAIN}"
      GIT_HTTPS_PASSWORD="${GIT_HTTPS_PASSWORD_MAIN}"
    else
      __msg_error "Commit received on a branch different than dev/main.. Aborting"
      exit "${FAILURE_CODE}"
    fi
}
#

apk add --no-cache \
        git \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir \
        awscli \
    && rm -rf /var/cache/apk/*

check_mandatory_vars
 
__msg_info "Checking if the push needs to happen on allowed branches"
check_branch
 
__msg_info "Pushing latest commit hash ${CI_COMMIT_SHA} to ${AWS_CODECOMMIT_REPOSITORY_URL}"
 
__msg_info "Creating local branch ${CI_COMMIT_REF_NAME}-${CI_JOB_ID}"
git checkout -b "${CI_COMMIT_REF_NAME}-${CI_JOB_ID}"
 

#############################################################
# Push recent commits to the target repository and exit
# with success code. If push fails, the script errors out
# with an exit code of 255
#############################################################

if git push "https://${GIT_HTTPS_USERNAME}:${GIT_HTTPS_PASSWORD}@${AWS_CODECOMMIT_REPOSITORY_URL}" "${CI_COMMIT_REF_NAME}-${CI_JOB_ID}":"${CI_COMMIT_REF_NAME}" --force; then
    __msg_info "Successfully pushed the commits to AWS Codecommit remote"
else
    __msg_error "Unable to push commits to AWS CodeCommit remote.. Failing"
    exit ${FAILURE_CODE}
fi